var editor = CKEDITOR.replace('editorForm', {
            fullPage: true,
            allowedContent: true,
            extraAllowedContent: '*{*}',
            extraPlugins: 'wysiwygarea,devtools',
            contentsCss: ['css/style.css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'],
            uiColor: '#FFFFF1',
            height: 500,
            width: 764,
            toolbar :
            [
                { name: 'buttons', 
                    items : [ 
                        'Source',
                        'Save', 
                        'Find',
                        'Replace',
                        'SpellChecker', 
                        'Link', 
                        'Unlink', 
                        'Image',
                        'HorizontalRule', 
                        'SpecialChar', 
                        'Bold',
                        'Italic',
                        'Underline',  
                        'Superscript',
                        'Subscript',
                        'Smiley',
                        'NumberedList',
                        'BulletedList', 
                        'Outdent', 
                        'Indent', 
                        'Blockquote', 
                        'Styles', 
                        'About'
                    ] 
                }
            ]
        });