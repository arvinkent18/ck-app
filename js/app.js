$(document).keydown(function(event) {
    if (!( String.fromCharCode(event.which).toLowerCase() == 'b' && event.ctrlKey) && !(event.which == 19)) return true;
    saveData();
    event.preventDefault();

    return false;
});
$('#ckeditorForm').on('submit', function(e) {
    saveData();
    e.preventDefault();
});
$(document).ready(function() {
    
    var renderHtml = function(someStuff) {
    var promise = new Promise(function(resolve, reject){
            setTimeout(function() {
            $.get("work.htm", function(data, status){
                $('#editorForm').val(data);
            })
            resolve({data: '123'});
            }, 1);
        });
        return promise;
    };    
    var reConfigurePosts = function(someStuff) {
    var promise = new Promise(function(resolve, reject){
        setTimeout(function() {
        var posts = editor.document.find('.posts');
        var separators = editor.document.find('.separators');

        for (i = 0; i < posts.count(); i++) {
            posts.getItem(i).setAttribute('id', 'post-id-' + i);

            var headerRadiosDel = editor.document.find('.del-buttons');
            headerRadiosDel.getItem(i).setAttribute('id','del-button-' + i);
            
            var headerRadiosTodo = editor.document.find('.todo-buttons');
            headerRadiosTodo.getItem(i).setAttribute('id','todo-button-' + i);;
            
            var headerRadiosDone = editor.document.find('.full-buttons');
            headerRadiosDone.getItem(i).setAttribute('id','full-button-' + i);
            
            var headerRadiosLean = editor.document.find('.lean-buttons');
            headerRadiosLean.getItem(i).setAttribute('id','lean-button-' + i); 
        }

        var postIds = [];

        var delButtons = editor.document.find('.del-buttons');
        console.log('Del Buttons: ' + delButtons.toArray());
        var delButtonsIds = [];
        var leanButtons = editor.document.find('.lean-buttons');
        console.log('Lean Buttons: ' + leanButtons.toArray());
        var leanButtonsIds = [];
        var fullButtons = editor.document.find('.full-buttons');
        console.log('Full Buttons: ' + fullButtons.toArray());
        var fullButtonsIds = [];
        var todoButtons = editor.document.find('.todo-buttons');
        console.log('Todo Buttons: ' + todoButtons.toArray());
        var todoButtonsIds = [];

        for (i = 0; i < delButtons.count(); i++) {
            currentPostId = posts.getItem(i).getAttribute('id');
            postIds.push(currentPostId);
            currentDelId = delButtons.getItem(i).getAttribute('id');
            delButtonsIds.push(currentDelId);
            currentLeanId = leanButtons.getItem(i).getAttribute('id');
            leanButtonsIds.push(currentLeanId);
            currentFullId = fullButtons.getItem(i).getAttribute('id');
            fullButtonsIds.push(currentFullId);
            currentTodoId = todoButtons.getItem(i).getAttribute('id');
            todoButtonsIds.push(currentTodoId);
        }  
        
        console.log("List of Delete Button Ids: " + delButtons)
        console.log("List of Todo Button Ids: " + todoButtons)
        console.log("List of Delete Button Ids: " + leanButtons)
        console.log("List of Full Button Ids: " + fullButtons)
        console.log("List of Posts: " + postIds);

        function softNumbers() {
          for(i = 0; i < posts.count(); i++) {
             var softNumbers = editor.document.find('.soft_post_num_col');
             softNumbers.getItem(i).setHtml(i+1);
          }
        }

        softNumbers();

        function countTasks() {
            var todo = 0;
            var lean = 0;
            var full = 0;

            todo = editor.document.find('.todo_').count();
            console.log('Number of Todo are: ' + todo);
            lean = editor.document.find('.lean_').count();
            console.log('Number of Lean are: ' + lean);
            full = editor.document.find('.full_').count();
            console.log('Number of Full are: ' + full+lean);

            $('#dialog').children().remove();
            $('#dialog').append('<p>' + todo + ' left to do <br />' + full + ' done(' + lean + ' lean)</p>');
            $('#dialog').dialog();  
        }

        countTasks();

        editor.addCommand( 'count-tasks', {
            exec: function(editor) {
                countTasks();
            }
        } );

        editor.setKeystroke( CKEDITOR.ALT + 49, 'count-tasks');

        CKEDITOR.on( 'dialogDefinition', function( ev ) {
            // Take the dialog name and its definition from the event data.
            var dialogName = ev.data.name;
            var dialogDefinition = ev.data.definition;
        
            // Check if the definition is from the dialog window you are interested in (the "Link" dialog window).
            if ( dialogName == 'find' ) {
                
                // Get a reference to the "Link Info" tab.
                //var infoTab = dialogDefinition.getContents( 'find' );
        
                // Set the default value for the URL field.
                //var urlField = infoTab.get( 'url' );
                //var urlField = infoTab.get('txtFindWordChk');
                //urlField['default'] = 'Regex Expression';
                //urlField[ 'default' ] = 'www.example.com';
            }
        });

        editor.document.on('keydown', function(evt) {
          if (evt.data.getKey() == 40 || evt.data.getKey() == 38) {
            var post = posts.toArray();
            console.log(evt.data.getTarget().getParents());
           // main:
            //for (i = 1; i < evt.data.getTarget().getChildren().$.length; i+=2) {
                //console.log(i);
                //var id = evt.data.getTarget().getChildren().$[i].getAttribute('id');
                //console.log('id is: ' + id)
                //inner:
               /* for (x = i-1; x < posts.count(); x++) {
                  console.log('x is: ' + x);
                  if (postIds[x] == id) {
                       console.log('post ' + post[x].getAttribute('id'));
                       break main;
                  }
                  else {
                    continue;
                  }
                }*/
               
                  //else {
                    //continue;
                  //}
                     
            //}
              
          }
        });

        editor.document.on('click', function(evt) {
           var targetElement = evt.data.getTarget();
           var selectedId = targetElement.getAttribute('id');
           var post = posts.toArray();
           var separator = separators.toArray();
           var postId = targetElement.getParents()[2].getAttribute('id');
           console.log(postId);

           editor.addCommand('insertComment', {
              exec: function(editor) {
                   console.log('working');
                  var range = editor.createRange(),
                  comment = CKEDITOR.dom.element.createFromHtml(
                    '<table class="HB" align="center" border="0" cellpadding="0" cellspacing="0" style="background:#fffff4;border-radius:9px;border:2px solid #44abab;width:90`%;">' + 
                    '<tbody>' + 
                    '<!--[if (gte mso 9)|(IE)]>' +
                    '<tr>' + 
                    '<td width="15" style="font-size:9px;">&nbsp;</td>' + 
                    '<td style="font-size:9px;">&nbsp;</td>;' +
                    '<td width="24" style="font-size:9px;">&nbsp;</td>' +
                    '</tr>' +
                    '<![endif]-->' +
                    '<tr>' +
                    '<td width="15">&nbsp;</td>' +
                    '<td style="font-family: Arial; font-size: 16px; color: #202020; line-height: 1.3">' +
                      '<p><img src="https://hbletter.com/memberphotos/HB25-thumbnail-mirrored.png" align="left">**' +
                    '</p></td>' +
                    '<td width="10" style="font-size:4px;">&nbsp;</td>' +
                    '</tr></tbody></table>' +
                    '<!--[if (gte mso 9)|(IE)]>' +
                    '<p "style=font-size: 12px; line-height: .75;">&nbsp;</p> ;' +
                    '<![endif]-->'
                  );
                  range.setStartAt(targetElement, CKEDITOR.POSITION_BEFORE_END);
                  range.collapse(true);
                  editor.editable().insertElement(comment, range);
              }
           });
           
           editor.setKeystroke( CKEDITOR.CTRL + 73, 'insertComment');

           editor.setKeystroke( CKEDITOR.CTRL + 83, 'save');

           for (i = 0; i < delButtonsIds.length; i++) {
               if (postIds[i] == postId) {
                   var prev_id;

                    $(window).scroll(function(e){
                       alert($(e.target).attr("id"));
                    });


                    editor.addCommand( 'delete', {
                        exec: function(editor) {
                            editor.document.getById(postId).remove();
                            countTasks();
                            softNumbers();
                        }
                    } );
                    
                    editor.addCommand( 'lean', {
                        exec: function( editor) {
                            editor.document.getById(postId).setAttribute('class', 'posts lean_');
                            countTasks();
                        }
                   } );
                
                   editor.addCommand( 'full', {
                        exec: function( editor) {
                            editor.document.getById(postId).setAttribute('class', 'posts full_');
                            countTasks();
                        }
                   } );
                    
                   editor.addCommand( 'todo', {
                        exec: function( editor) {
                            editor.document.getById(postId).setAttribute('class', 'posts todo_');
                            countTasks();
                        }
                   } );
                   
                   editor.setKeystroke( CKEDITOR.ALT + 68, 'delete');
                   editor.setKeystroke( CKEDITOR.ALT + 76, 'lean');
                   editor.setKeystroke( CKEDITOR.ALT + 70, 'full');
                   editor.setKeystroke( CKEDITOR.ALT + 84, 'todo');
               }
              
               if (selectedId == delButtonsIds[i]) {
                   post[i].remove();
                   separator[i].remove();
                   countTasks();
                   softNumbers();
                   break;
               }
               else if (selectedId == leanButtonsIds[i]) {
                   post[i].setAttribute('class', 'posts lean_');
                   leanButtons.getItem(i).setAttribute('class', 'lean-buttons active'); 
                   editor.execCommand('save');
                   countTasks();
                   break;
               }
               else if (selectedId == fullButtonsIds[i]) {
                   post[i].setAttribute('class', 'posts full_');
                   fullButtons.getItem(i).setAttribute('class', 'full-buttons active');
                   editor.execCommand('save');
                   countTasks();
                   break;
               }
               else if (selectedId == todoButtonsIds[i]) {
                   post[i].setAttribute('class', 'posts todo_');
                   todoButtons.getItem(i).setAttribute('class', 'todo-buttons active');
                   editor.execCommand('save');
                   countTasks();
                   break;
               }
               else {
                   continue;
               }
           }
        });

        resolve({newData: someStuff.data + ' some more data'});
        }, 2000);
    });
    return promise;
    };
    renderHtml()
    .then(reConfigurePosts)
    
});