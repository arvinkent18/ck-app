@echo OFF
    start C:\"Program Files (x86)"\Google\Chrome\Application\chrome.exe --allow-file-access-from-files file:///C:/Users/arvinkent18/Desktop/editor-app/index.htm
    setlocal enableextensions disabledelayedexpansion

    set "source=C:\Users\arvinkent18\Downloads\*.htm"
    set "target=C:\Users\arvinkent18\Desktop\editor-app\"
    set "searchString=work"
    :start
    set "found="
    for /f "delims=" %%a in ('
        findstr /m /i /l /c:"%searchString%" "%source%" 2^>nul 
    ') do (
        if not defined found set "found=1"
        move /Y "%%a" "%target%"
    )

    if not defined found (
        echo Searching...
    )

    goto start